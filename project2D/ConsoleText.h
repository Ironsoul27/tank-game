#pragma once
#include <iostream>
#include "DataManagers.h"
#include <vector>
#include <string>

namespace aie {
	class Renderer2D;
}

class EntityText;
class Tank;

namespace aie {
	class Renderer2D;
}

using std::vector;
using std::string;

class ConsoleText : public DataManagers
{
public:
	typedef std::tuple<string, string, bool> TextTuple;

	ConsoleText();
	~ConsoleText();


	void const Draw(aie::Renderer2D* m_2dRenderer, Tank& tank);
	bool const UpdateConsoleText(vector<TextTuple> &textList, float deltaTime);
	void const AddEntityText(EntityText* ET);

private :

	EntityText* m_EntityText;

	bool m_textAssigned = false;
	size_t m_textIterator = 0;
};

