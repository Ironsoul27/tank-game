#pragma once
#include "DataManagers.h"
#include <vector>
#include <memory>

class Bullets;

using std::vector;


class BulletManager : public DataManagers {
public:

	BulletManager();
	virtual ~BulletManager();

	void const Startup();

	std::shared_ptr<vector<Bullets*>> GetBullets();
	Bullets* const ConstructBullet(float speed, float b_timer);
	void const UpdateBullets(float deltaTime);
	bool const CheckForBunny(size_t j, size_t i);

	//void const AddBunnyManager(BunnyManager* BM);


private:

	std::shared_ptr<vector<Bullets*>>	m_Bullets;
	//BunnyManager* m_Bunny_Manager = nullptr;

};