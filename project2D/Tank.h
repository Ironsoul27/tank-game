#pragma once
#include "SpriteObject.h"
#include "Turret.h"
#include "Sphere.h"
#include "Input.h"
#include <memory>
//#include "DataManagers.h"

class BulletManager;
class BunnyEnemy;


//#include "BulletManager.h" // from BM // then EM


class Tank : public SpriteObject {
public:

	Tank();
	Tank(float m_speed, float r_multplier);
	virtual ~Tank();

	float const getSpeed();
	float const getRotationMultiplier();
	Vector3& GetRadarFacing();

	// load sprites and attach turret
	void setup(std::shared_ptr<aie::Texture>& tankImage, std::shared_ptr<aie::Texture>& turretImage, float tankSpeed);
	void const UpdateRadar(float deltaTime, vector<BunnyEnemy*>& m_Bunnies);
	void const ShootBullet(float deltaTime);
	void const AddBulletManager(BulletManager* BM);


	// update tank/turret movement
	virtual void onUpdate(float deltaTime);
	//virtual void onDraw(aie::Renderer2D* renderer);

	Turret m_Turret;
	//SpriteObject m_Tank;

private:

	BulletManager* BM = nullptr;

	// Bunny Radar
	BunnyEnemy* ClosestBunny = nullptr;
	Vector3		m_BR_Facing;
	size_t		m_CheckedBunnies = 0;
	float		m_BRTimer = 0;
	float		m_ClosestDistance = FLT_MAX;

	float		m_BulletSpawnRate = 0.01f;
	float		m_BulletSpawnTimer = 1;
	float		m_BulletSpeed = 1000;
	float		m_BulletLifespan = 1;
	float		m_Bullet;
	float		m_Speed;
	float		m_Rot_Multiplier;
};