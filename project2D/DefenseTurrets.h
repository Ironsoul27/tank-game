#pragma once
#include "SpriteObject.h"
#include "DataManagers.h"

class BunnyEnemy;

class DefenseTurrets : public DataManagers
{
public:
	DefenseTurrets();
	~DefenseTurrets();

	void const Setup(std::shared_ptr<aie::Texture>& texture, float scale, float posX, float posY);
	void const SearchForTarget();
	void const RotateToTarget();

	virtual void OnUpdate(float deltaTime);

	SpriteObject m_Turret;

private:
	void const FireAtTarget();

#define		SEARCHRADIUS 200000;

	BunnyEnemy*		m_Target = nullptr;
	Vector3			m_TurretFacing;
	float			m_BarrelLength = 60;
	float			m_BulletSpeed = 1000;
	float			m_BulletLifetime = 2;
	float			m_SearchRadius = SEARCHRADIUS;
	float			m_ClosestBunnyDist = SEARCHRADIUS;
	float			m_FireTimer = 0;
};

