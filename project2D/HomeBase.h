#pragma once
#include "SpriteObject.h"
#include "DataManagers.h"

class HomeBase : public DataManagers
{
public:
	HomeBase();
	~HomeBase();

	bool const takeDamage(float damage);
	float const getHealth() const;
	void const setup(/*std::shared_ptr<aie::Texture>& texture,*/ float scale, float posX, float posY);

	virtual void draw(aie::Renderer2D* renderer);

	SpriteObject m_HomeBase;

private:

	float m_Health = 300;
};

