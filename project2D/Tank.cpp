#include "Tank.h"
#include "BulletManager.h"
#include "BunnyEnemy.h"
#include "Bullets.h"

Tank::Tank() {
	m_Speed = 500;
	m_Rot_Multiplier = 5;
}

Tank::Tank(float m_speed, float r_multiplier)
{
	this->m_Speed = m_speed;
	this->m_Rot_Multiplier = r_multiplier;
}

Tank::~Tank()
{
}


float const Tank::getSpeed() {
	return m_Speed;
}

float const Tank::getRotationMultiplier() {
	return m_Rot_Multiplier;
}

Vector3 & Tank::GetRadarFacing()
{
	return m_BR_Facing;
}


void Tank::setup(std::shared_ptr<aie::Texture>& tankImage, std::shared_ptr<aie::Texture>& turretImage, float tankSpeed) {
	setTexture(tankImage);
	m_Turret.setTexture(turretImage);
	addChild(&m_Turret);
	setVelocity(tankSpeed);
}

void Tank::onUpdate(float deltaTime) {

	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_A))
		rotate(deltaTime * m_Rot_Multiplier);
	if (input->isKeyDown(aie::INPUT_KEY_D))
		rotate(-deltaTime * m_Rot_Multiplier);


	if (input->isKeyDown(aie::INPUT_KEY_W)) {
		auto facing = getLocalTransform()[1] *
			deltaTime * getVelocity();
		translate(facing.x, facing.y);
	}

	if (input->isKeyDown(aie::INPUT_KEY_S)) {
		auto facing = getLocalTransform()[1] *
			-deltaTime * getVelocity();
		translate(facing.x, facing.y);
	}

	if (input->isKeyDown(aie::INPUT_KEY_LEFT))
		m_Turret.rotate(deltaTime * m_Turret.getRotationMultiplier());
	if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
		m_Turret.rotate(-deltaTime * m_Turret.getRotationMultiplier());


	if (input->isKeyDown(aie::INPUT_KEY_SPACE)) { // shoot bullet // swap input type for burst or single fire type
		m_BulletSpawnTimer += deltaTime;

		if (m_BulletSpawnTimer > m_BulletSpawnRate) {
			ShootBullet(deltaTime);
			m_BulletSpawnTimer = 0;
		}
	}

	if (input->isKeyUp(aie::INPUT_KEY_SPACE))
		m_BulletSpawnTimer = 1;

}

void const Tank::UpdateRadar(float deltaTime, vector<BunnyEnemy*>& m_Bunnies)
{
	// Loop through 1/10th of the total bunnies a frame, comparing closest float value to the tank and reference to the bunny who was closest.
	// Once a second has passed perform calculation to get facing vector to the nearest bunny


	m_BRTimer += deltaTime;
	size_t ArraySize = m_Bunnies.size();

	if (m_CheckedBunnies != ArraySize) {
		float temp;
		size_t iterateOver;

		if (ArraySize < 10) // if there are less than 10 bunnies, only iterate to the array's size
			iterateOver = ArraySize;
		else
			iterateOver = (ArraySize / 10);

		for (size_t i = m_CheckedBunnies; i < (m_CheckedBunnies + iterateOver); ++i) {
			if (i >= ArraySize) // catch to stop elements from going out of range
				break;

			temp = getLocalTransform()[2].distanceSqr(m_Bunnies[i]->getLocalTransform()[2]);
			
			if (!m_Bunnies[i]->isCaught() && temp < m_ClosestDistance) {
				m_ClosestDistance = temp;
				ClosestBunny = m_Bunnies[i];
			}
			m_CheckedBunnies++;
		}
	}

	if (m_BRTimer >= 1 && ClosestBunny != nullptr) {
			m_BR_Facing = ClosestBunny->getLocalTransform()[2] - getLocalTransform()[2];
			m_BR_Facing.normalise();
			m_BR_Facing = m_BR_Facing * 60;	
		
		m_BRTimer = m_CheckedBunnies = 0.f;
		m_ClosestDistance = FLT_MAX;
	}
}


void const Tank::ShootBullet(float deltaTime) // Shoot bullets at constant rate through deltaTime
{
	Bullets* m_bullet = BM->ConstructBullet(m_BulletSpeed, m_BulletLifespan);

	m_bullet->setLocalTransform(m_Turret.getGlobalTransform());

	auto facing = m_bullet->getLocalTransform()[1];
	m_bullet->translate(facing.x * 60, facing.y * 60);

	BM->GetBullets()->push_back(m_bullet);
	
}


void const Tank::AddBulletManager(BulletManager* BM)
{
	this->BM = BM;
}