#include "ResourceManager.h"



ResourceManager::ResourceManager()
{
}


ResourceManager::~ResourceManager()
{
}

void const ResourceManager::StartUp()
{
	m_enemy_tex = std::make_shared<aie::Texture>("../textures/bunny.png");
	m_plane_tex = std::make_shared<aie::Texture>("../textures/Plane.png");
	m_tank_tex = std::make_shared<aie::Texture>("../textures/tank.png");
	m_turret_tex = std::make_shared<aie::Texture>("../textures/gunturret.png");
	m_carrotBullet_tex = std::make_shared<aie::Texture>("../textures/Carrot_Small.png");

	m_Font = new aie::Font("./font/consolas.ttf", 32);
	m_ConsoleFont = new aie::Font("./font/yoster.ttf", 32);

	m_Textures = {
		{"EnemyTex", m_enemy_tex },
		{"PlaneTex", m_plane_tex },
		{"TankTex", m_tank_tex },
		{"TurretTex", m_turret_tex },
		{"BulletTex", m_carrotBullet_tex }
	};

	m_Fonts = {
		{"DefaultFont", m_Font },
		{"ConsoleFont", m_ConsoleFont}
	};

	string Blank = "";
	string LB = "Fluff";
	string Unknown = "???????????";
	string CO = "Communications Officer";
	string Player = "You";

	vector<TextTuple>	m_TalkText_1 = { make_tuple(Player ,"Wait...  What?", false) ,
										make_tuple(Unknown ,"Hello, my name is Fluff and I'm the leader of this rabble of rabbits", false) ,
										make_tuple(LB ,"What, you thought this was just a small tutorial project?", false) ,
										make_tuple(LB ,"FOOL, I have already programmed over 5 hours of content! (Not really :D)", false) ,
										make_tuple(LB ,"Now, do you know why I am the leader of this group?", false) ,
										make_tuple(LB ,"It's because I was the first bunny in the arra--", true) ,
										make_tuple(LB ,"I MEAN my ears were 5 micropixels longer than everyone elses", false) ,
										make_tuple(LB ,"Natural selection, baby", false) ,
										make_tuple(LB ,"Now, while I must admit those carrots were rather delicious", false) ,
										make_tuple(LB ,"I'm afraid the situation has now... changed", false) ,
										make_tuple(LB ,"And so now I must regret to inform you that we will now proceed with tearing apart your giant metal cocoon", false) ,
										make_tuple(LB ,"And using your technology to escape this void and conquer the world!", false) ,
										make_tuple(Unknown ,"QUICK, SHOOT TO CAPTURE HIM!    DO IT NOW!", true)
	};

	vector<TextTuple>	m_TalkText_2 = { make_tuple(Blank ,"", false) ,
										make_tuple(LB ,"HAHA did you not think that I would have foreseen this? ", false) ,
										make_tuple(LB ,"I mean I'm standing in front of the barrel for Petes sake! Your weapon is now useless!", false) ,
										make_tuple(Unknown ,"Ahh sorry guns jammed, they must've done something", false) ,
										make_tuple(Player ,"Wait, who are YOU?", false) ,
										make_tuple(Unknown ,"I'm your communications officer, I've literally been sitting next to you this entire time", false) ,
										make_tuple(CO ,"Anyway I've called in air support, should be here any second now", true)
	};

	vector<TextTuple>	m_TalkText_3 = { make_tuple(Player ,"	  We      are      so      dea--", true) ,
										make_tuple(LB ,"AHHHHHHHHHHHHHHHH NOOOOOOOO, THE HAPPINESS AND ENTHUSIASM! AHHHHHHHHHHHH IT BUUUUUURNS!", false) ,
										make_tuple(CO ,"Well I'll be", false) ,
										make_tuple(CO ,"Sir, main systems are back and operational, let's retreat back to home base", false) ,
										make_tuple(Player ,"Did I always have a CO with a Canadian accent?", false) ,
										make_tuple(LB ,"Grrrr fall back my fluffy brethren, prepare ourselves for all out carrot war", false)

	};

	vector<TextTuple>	m_TalkText_4 = { make_tuple(CO ,"OK sir, The tyrannical bunnies are about to begin their assault", false) ,
										 make_tuple(CO ,"We've just recieved a fresh supply of equipment, along with new mobile turrets", false) ,
										 make_tuple(CO ,"Place them around strategic locations to release carrots on those fluffy buggers", false) ,
										 make_tuple(CO ,"We need to protect the home base at all cost", false) ,
										 make_tuple(CO ,"Even if it costs us our very lives...", false) ,
										 make_tuple(Player ,"...", false) ,
										 make_tuple(CO ,"...", false) ,
										 make_tuple(Player ,"Actually yeah nah I wouldn't go that far  ", true) ,
										 make_tuple(CO ,"Yeah nah soz just wanted to sound all cool and all  ", true) ,
										 make_tuple(Player ,"Yeah well like, I've kind of got plans after this  ", true) ,
										 make_tuple(Player ,"Also I'm feeling pretty good about the lottery tickets this weeke--  ", true) ,
										 make_tuple(CO ,"AHHHHHH THEY'RE COMING!", false)

	};

	m_BunnyText = {
		{"Text_1", m_TalkText_1},
		{"Text_2", m_TalkText_2},
		{"Text_3", m_TalkText_3},
		{"Text_4", m_TalkText_4}
	};
}

void const ResourceManager::ShutDown()
{
	delete m_Font;
	delete m_ConsoleFont;
}

std::shared_ptr<aie::Texture>& ResourceManager::GetTexture(string key)
{
	return m_Textures.at(key);
}

aie::Font * ResourceManager::GetFont(string key)
{
	return m_Fonts.at(key);
}

vector<TextTuple> ResourceManager::GetTexts(string key)
{
	return m_BunnyText.at(key);
}
