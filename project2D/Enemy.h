#pragma once
#include "SpriteObject.h"

class Enemy : public SpriteObject
{
public:
	Enemy();
	Enemy(float m_health, float attackDamage, float m_speed);
	~Enemy();

	virtual float const getSpeed();
	virtual float const getAttack();
	virtual float const getHealth();
	virtual bool const takeDamage(float damage);	
	virtual void const setSpeed(float speed);
	void const lerpPosition(const Vector3& other);


protected:

	float m_health;
	float m_attackDamage;
	float m_speed;
};

