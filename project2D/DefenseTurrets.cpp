#include "DefenseTurrets.h"
#include "EntityManager.h"
#include "Bullets.h"
#include "BunnyEnemy.h"
#include "BulletManager.h"



DefenseTurrets::DefenseTurrets()
{
}


DefenseTurrets::~DefenseTurrets()
{
}

void const DefenseTurrets::Setup(std::shared_ptr<aie::Texture>& texture, float scale, float posX, float posY)
{
	m_Turret.setTexture(texture);
	m_Turret.setScale(scale, scale);
	m_Turret.setPosition(posX, posY);
}

void DefenseTurrets::OnUpdate(float deltaTime)
{
	SearchForTarget();

	if (m_Target != nullptr) { // target is nullptr when not assigned
		m_FireTimer += deltaTime;
		RotateToTarget();

		if (m_FireTimer > .3f) {
			FireAtTarget();
			m_FireTimer = 0;
		}
	}
}


void const DefenseTurrets::FireAtTarget()
{
	Bullets* bullet = m_BulletManager->ConstructBullet(m_BulletSpeed, m_BulletLifetime);
	
	bullet->setLocalTransform(m_Turret.getGlobalTransform());

	auto facing = bullet->getLocalTransform()[1];
	bullet->translate(facing.x * m_BarrelLength, facing.y * m_BarrelLength);

	m_BulletManager->GetBullets()->push_back(bullet);
}


void const DefenseTurrets::SearchForTarget() // Take note of closest bunny to us, and check if they're also within range of the turret, if so mark them as the target
{
	bool m_HaveFoundTarget = false;
	float temp;
	m_ClosestBunnyDist = SEARCHRADIUS;

	for (auto bunny : m_EntityManager->GetBunnies()) { 

		if (bunny->isCaught())
			continue;

		temp = m_Turret.m_localTransform[2].distanceSqr(bunny->getLocalTransform()[2]);

		if (temp < m_SearchRadius) {	

			if (m_Target != nullptr && m_Target != bunny) { // If we already have a target and the test target is not already our current target
				if (temp < m_ClosestBunnyDist) { // If they're closer than the current closest bunny
					m_ClosestBunnyDist = temp;
					m_Target = bunny;
				}
			}
			else if (m_Target != bunny) // Default option for target = null
				m_Target = bunny;

			m_HaveFoundTarget = true;
		}		
	}
	if (!m_HaveFoundTarget)
		m_Target = nullptr;
}

void const DefenseTurrets::RotateToTarget() // I can already hear the sound of other programmers mouths dropping in horror looking at this function 
											// Not quite what I was orignally intending, however it achieves the same effect as getting rotation angles, albeit without the ability to easily lerp between 2 points
{
	if (m_Target != nullptr) {

		Vector3 TargetVec = m_Target->getGlobalTransform()[2] - m_Turret.getGlobalTransform()[2];
		Vector3 Perp = TargetVec.getPerpendicularRH();

		TargetVec.normalise();
		Perp.normalise();

		m_Turret.setLocalTransformFacing(TargetVec);
		m_Turret.setLocalTransformPerpindicularFacing(Perp);
		m_Turret.rotate(-1.5708); // Rotate round -90 degrees
	}
}
