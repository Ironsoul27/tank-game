#include "DataManagers.h"
#include "GameManager.h"
#include "Application.h"
#include "ResourceManager.h"
#include "EntityManager.h"
#include "BulletManager.h"


DataManagers::DataManagers()
{
}


DataManagers::~DataManagers()
{
}

void const DataManagers::AddResourceManager(ResourceManager * RM)
{
	m_ResourceManager = RM;
}

void const DataManagers::AddGameState(GameManager * GM)
{
	m_GameManager = GM;
}

void const DataManagers::AddApplication(aie::Application * m_App)
{
	this->m_App = m_App;
}

void const DataManagers::AddBulletManager(BulletManager * BM)
{
	m_BulletManager = BM;
}

void const DataManagers::AddEntityManager(EntityManager * EM)
{
	m_EntityManager = EM;
}
