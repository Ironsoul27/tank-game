#pragma once
#include "SpriteObject.h"


class Turret : public SpriteObject {
public:
	Turret();
	Turret(float r_multipler);
	virtual ~Turret();

	float const getRotationMultiplier();

private:

	float r_multiplier;
};

