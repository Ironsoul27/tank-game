#include "WaveManager.h"
#include "BunnyEnemy.h"
#include "HomeBase.h"
#include "EntityManager.h"
#include <sstream>

WaveManager::WaveManager()
{
}

WaveManager::WaveManager(unsigned int maxWaves) : m_MaxWaves(maxWaves)
{
}


WaveManager::~WaveManager()
{
}

size_t const WaveManager::GetCurrentWave()
{
	return m_CurrentWave;
}

string const WaveManager::GetCurremtWaveText()
{
	std::stringstream s;
	string m_Text = "Current Wave: ";
	s << m_Text << m_CurrentWave;
	return s.str();
}

void const WaveManager::SetNextWave()
{
	m_CurrentWave++;
	m_CalcSpawnLoc = true;
}

void const WaveManager::SpawnEnemy()
{
	for (size_t i = 0; i < (m_CurrentWave + 1)* 2; i++) {
		for (Vector3 spawnLoc : m_SpawnLocations) {
			BunnyEnemy* bunny = m_EntityManager->ConstructBunny(spawnLoc.x, spawnLoc.y);
		
			m_EntityManager->GetBunnies().push_back(bunny);
		}
	}
}

bool const WaveManager::SetSpawns(HomeBase& HB, size_t numSpawnLocs)
{
	Matrix3 m = HB.m_HomeBase.getGlobalTransform();

	for (size_t i = 0; i <= numSpawnLocs; i++) {
	
		float rand_F = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 2.0f));

		m.rotateZ(/*fmod(5.4f, rand())*/ rand_F);
		auto facing = m[1];
		m.translate(facing.x * 2000, facing.y * 2000);

		m_SpawnLocations.push_back(m[2]);
	}
	return true;
}

bool const WaveManager::Startup(HomeBase& HB, size_t numSpawnLocs)
{
	if (m_CalcSpawnLoc) {
		m_SpawnLocations.clear();

		if (SetSpawns(HB, numSpawnLocs)) {
			m_CalcSpawnLoc = false;
			return true;
		}
		else
			return false;
	}
	return false;
}

bool const WaveManager::HasPlayerWon()
{
	if (m_CurrentWave == m_MaxWaves)
		return true;
	else
		return false;
}