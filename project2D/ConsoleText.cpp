#include "ConsoleText.h"
#include "ResourceManager.h"
#include "Application.h"
#include "EntityText.h"

ConsoleText::ConsoleText() 
{
}


ConsoleText::~ConsoleText()
{
}

void const ConsoleText::Draw(aie::Renderer2D* m_2dRenderer, Tank& tank)
{
	// Text Box
	m_2dRenderer->setRenderColour(255.f, 255.f, 255.f, .7f);
	m_2dRenderer->drawBox(tank.getLocalTransform()[2].x, tank.getLocalTransform()[2].y - (m_App->getWindowHeight() / 2.3f), m_App->getWindowWidth(), m_App->getWindowHeight() / 5.f);

	// Author
	m_2dRenderer->setRenderColour(255.f, 255.f, 255.f, 1);
	m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), m_EntityText->getAuthor().c_str(), tank.getLocalTransform()[2].x + m_App->getWindowWidth() / -2.0f, tank.getLocalTransform()[2].y - (m_App->getWindowHeight() / 3.09f));

	// Main Text
	m_2dRenderer->setRenderColour(0, 0, 0);
	m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), m_EntityText->getText().c_str(), tank.getLocalTransform()[2].x + m_App->getWindowWidth() / -2.0f, tank.getLocalTransform()[2].y - (m_App->getWindowHeight() / 2.4f));

}


bool const ConsoleText::UpdateConsoleText(vector<TextTuple>& textList, float deltaTime)
{
	if (!m_textAssigned && m_EntityText->setText(textList[m_textIterator]))
		m_textAssigned = true;

	if (m_EntityText->updateText(deltaTime)) {
		m_textAssigned = false;

		if (m_textIterator != textList.size() - 1) { // if hit we have finished the line and it's timer, now check if we've hit the end of the list
			m_textIterator++;
			return false;
		}
		else {						//if hit the end of the list reset values and exit text loop
			m_textIterator = 0;
			return true;
		}
		return true;
	}
	return false;
}

void const ConsoleText::AddEntityText(EntityText * ET)
{
	m_EntityText = ET;
}

