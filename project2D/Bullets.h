#pragma once
#include "SpriteObject.h"
//#include "DataManagers.h"
//#include "EntityManager.h"
//#include "BunnyEnemy.h"

//class BunnyEnemy;


class Bullets : public SpriteObject
{
public:


	Bullets(std::shared_ptr<aie::Texture>& texture, float m_speed, float b_timer);
	virtual ~Bullets();

	float const getSpeed();
	float const getTimer();

	virtual void onUpdate(float deltaTime);

private:

	float m_speed;
	float b_timer;
};




