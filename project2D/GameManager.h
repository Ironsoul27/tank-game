#pragma once

class GameManager
{
public:
	GameManager();
	~GameManager();

	int const getGameState() const;
	void const setGameState(int gameState);

	int const getSE_State() const;
	void const setSE_State(int SE_State);

	int const getHD_State() const;
	void const setHD_State(int HD_State);

private:

	int gameState = 0;
	int HD_State = 0;
	int SE_State = 0;

};

