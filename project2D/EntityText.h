#pragma once
#include "Renderer2D.h"
#include "Tank.h"
#include <iostream>
#include <string>
#include <tuple>

using std::string;
using std::tuple;
using std::get;

class EntityText
{
public:
	EntityText();
	~EntityText();

	typedef std::tuple<string, string, bool> TextTuple;

#define NextTextInterval 1;

	bool const setText(TextTuple& textPair);
	bool const setText(string& author, string& text, float timer, bool instant);
	void const setTimer(float timer);
	string const getText();
	string const getAuthor();
	float const getTimer() const;
	bool const updateText(float deltaTime);


private:
	bool const writeToDisplayText();

	bool m_textFinished = false;
	bool m_instantReturn;

	string currentText; // text which is currently loaded in order to be inserted into the displaytext
	string displayText; // text to display
	string author;

	float m_CharTimer = 0;	   // time between each character update // set inside cpp
	float m_NextTextTimer = NextTextInterval; //  time between moving onto next sentence after the current sentence has finished being written

	size_t m_charIterator = 0;
};

