﻿#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "SpriteObject.h"
#include "Events.h"
//#include <Vector2.h>
//#include <Vector3.h>
#include <vector>
#include <tuple>
#include <memory>

class GameManager;
class ResourceManager;
class BulletManager;
class EntityManager;
class WaveManager;

class WaveEvent;
class ScriptedEvents;
class ConsoleText;
class EntityText;



using std::vector;
using std::string;
using std::tuple;
using std::stringstream;

class Application2D : public aie::Application {
public:

	Application2D();
	virtual ~Application2D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();


protected:

	typedef std::tuple<string, string, bool> TextTuple;

	void const updateDebugText(string& text, unsigned int check, aie::Input* input);
	//void const SpawnToSSCoordinates(aie::Input* input);

	aie::Renderer2D*	m_2dRenderer;

	// Data Managers
	GameManager* m_GameState;
	WaveManager* m_Wave_Manager;
	BulletManager* m_Bullet_Manager;
	ResourceManager* m_Resource_Manager;
	EntityManager* m_Entity_Manager;

	WaveEvent* m_WaveEvent;
	ScriptedEvents m_ScriptedEvents;
	ConsoleText* m_ConsoleText;
	EntityText* m_EntityText;



	//std::shared_ptr<vector<Bullets*>>		m_Bullets;


	vector<string>		m_BunnyTexts = { "We all share the same texture, we are one",
										"How are we even talking?",
										"OOOF TOP GUY YOU'RE SO HEAVY",
										"Hi",
										"Wanna hear a joke? I promise it'll be super bunny",
										"HAVE YOU EVEN SEEN MY WARFACE",
										"Welcome to the void, military man",
										"hELP mY cAPS lOCK iS bROKEN",
										"Form is emptiness, emptiness is form",
										"Talk to the other bunny, he knows things, he even thinks things",
										"What are we even talking about?",
										"I know my times tables up to 11's"

	};

	vector<string>		m_BunnyTaunts = { "Mmmm Hmmm",
										  "What he said",
										  "PLEASE MARRY ME FLUFF",
										  "I'm hungry",
										  "DEATH TO ERATH",
										  "OUR FLUFFY LEADER!",
										  //"¯\_(ツ)_/¯",
										  "I can recite red riding hood backwards",
										  "YOU'RE THE BEST FLUFF!"
	
	};

#define ENEMYCOUNT 30
#define BUNNYSCALE .3f
#define BUNNYSPAWNOFFSET 500
#define TEXTOFFSET 45.0


	enum GameStates { 
		StartGame,
		ScriptedEvents,
		HomeDefence
	};

	enum HBDefence {
		Default,
		Transition,
		Intermission,
		Win,
		Lose,
		Waves
	};

	enum TextOutput {
		e_BulletTimer,
		e_BulletAxis,
		e_NumBullets,
		e_NumEnemies,
		e_MouseXY
	};


	string	 b_Axis;
	string	 bulletTimer;
	string	 numBullets;
	string	 numEnemies;
	string	 mouseXY;

	float	 m_textBasis;
	float	 m_bunnyHealth;
	float	 m_bunnySpeed;
	float	 m_tankSpeed;
};