#include "SpriteObject.h"
#include "Texture.h"


SpriteObject::SpriteObject()
{
}

bool SpriteObject::load(const char* filename) {
	m_texture = std::make_shared<aie::Texture>(aie::Texture(filename));
	return m_texture != nullptr;
}

void SpriteObject::onDraw(aie::Renderer2D* renderer) {
	renderer->drawSpriteTransformed3x3(m_texture.get(),
		(float*)&m_globalTransform);
}

bool SpriteObject::setTexture(std::shared_ptr<aie::Texture>& texture) {
	m_texture = texture;
	return m_texture.get() != nullptr; // .get() to grab raw pointer from shared_ptr
}

Vector2& SpriteObject::getVelocity()
{
	return Velocity;
}

void SpriteObject::setVelocity(const float scalar)
{
	Velocity.x = scalar;
	Velocity.y = scalar;
}

