#pragma once

class GameManager;
class ResourceManager;
class BulletManager;
class EntityManager;

namespace aie {
	class Application;
}


class DataManagers
{
public:
	DataManagers();
	~DataManagers();

	friend class ConsoleText;
	friend class ScriptedEvents;
	friend class WaveEvent;
	friend class BulletManager;
	friend class DefenseTurrets;
	friend class BunnyManager;
	friend class EntityManager;
	friend class WaveManager;
	friend class Events;

	void const AddResourceManager(ResourceManager* RM);
	void const AddGameState(GameManager* GM);
	void const AddApplication(aie::Application* m_App);
	void const AddBulletManager(BulletManager* BM);
	void const AddEntityManager(EntityManager* EM);


	enum GameStates {
		StartGame,
		ScriptedEvents,
		HomeDefence
	};

	enum SE_Enum {
		BunniesPositions,
		BunniesMoving,
		LeaderMoving,
		Introductions,
		ShootAttempt,
		PlaneFlyby,
		Ending
	};

	enum HBDefence {
		Default,
		Transition,
		Intermission,
		Win,
		Lose,
		Waves
	};


private:

	aie::Application* m_App = nullptr;
	GameManager* m_GameManager = nullptr;
	ResourceManager* m_ResourceManager = nullptr;
	EntityManager* m_EntityManager = nullptr;
	BulletManager* m_BulletManager = nullptr;

};

