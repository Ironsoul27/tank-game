#include "EntityManager.h"
#include "HomeBase.h"
#include "Tank.h"
#include "DefenseTurrets.h"
#include "BunnyEnemy.h"
#include "ResourceManager.h"


EntityManager::EntityManager()
{
}


EntityManager::~EntityManager()
{
}

Tank& EntityManager::GetPlayer()
{
	return m_Tank;
}

HomeBase& EntityManager::GetHomeBase()
{
	return m_HomeBase;
}

vector<DefenseTurrets*>& EntityManager::GetTurrets()
{
	return m_DefenseTurrets;
}

size_t const EntityManager::GetMaxTurrets() const
{
	return m_MaxNumTurrets;
}

vector<BunnyEnemy*>& EntityManager::GetBunnies()
{
	return m_Bunnies;
}

BunnyEnemy * EntityManager::ConstructBunny(float posX, float posY)
{
	BunnyEnemy* bunny = new BunnyEnemy();
	bunny->setTexture(m_ResourceManager->GetTexture("EnemyTex"));
	bunny->setScale(.3f, .3f);
	bunny->setPosition(posX, posY);
	return bunny;
}

void const EntityManager::IncrementCaughtBunnies()
{
	m_CaughtBunnies++;
}
