#include "EntityText.h"
#include <iostream>
#include <string>


EntityText::EntityText()
{
}


EntityText::~EntityText()
{
}


bool const EntityText::setText(TextTuple& textPair) 
{
	displayText.clear();
	author = std::get<0>(textPair);
	currentText = std::get<1>(textPair);
	m_instantReturn = std::get<2>(textPair);
	return true;
}


bool const EntityText::setText(string& author, string& text, float timer = 0, bool instant = false)
{
	displayText.clear();
	this->author = author;
	currentText = text;
	m_NextTextTimer = timer;
	m_instantReturn = instant;
	return true;
}


void const EntityText::setTimer(float timer)
{
	m_NextTextTimer = timer;
}

string const EntityText::getText()
{
	return displayText;
}

string const EntityText::getAuthor()
{
	return author;
}

float const EntityText::getTimer() const
{
	return m_CharTimer;
}

bool const EntityText::updateText(float deltaTime)
{
	if (!m_textFinished) {
		m_CharTimer += deltaTime;

		if (m_CharTimer > .04f) {

			if (!writeToDisplayText()) {
				m_CharTimer = 0;
				return false;
			}
			else 
				m_textFinished = true;
		}
	}
	else
		if (m_instantReturn) { // Return true to load next line if the current line was set to instantly return
			m_textFinished = false;
			return true;
		}
		else { // Else return once the delay timer has expired
			m_NextTextTimer -= deltaTime;
			if (m_NextTextTimer <= 0) {

				m_textFinished = false;
					m_NextTextTimer = NextTextInterval;
				return true;
			}
		}

	return false; 
}

bool const EntityText::writeToDisplayText()
{
	if (displayText.compare(currentText) != 0) {

		displayText.insert(displayText.begin() + m_charIterator, currentText[m_charIterator]);
		m_charIterator++;
		return false;
	}
	else {
		m_charIterator = 0;
		return true;
	}
}