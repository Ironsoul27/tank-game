#include "Enemy.h"

Enemy::Enemy()
{
	m_health = 100;
	m_speed = .009f;
}

Enemy::Enemy(float m_health, float m_attackDamage, float m_speed)
{
	this->m_health = m_health;
	this->m_attackDamage = m_attackDamage;
	this->m_speed = m_speed;
}


Enemy::~Enemy()
{
}


float const Enemy::getSpeed()
{
	return m_speed;
}

void const Enemy::setSpeed(float speed)
{
	m_speed = speed;
}

void const Enemy::lerpPosition(const Vector3 & other)
{
	auto lerpedPosition = getLocalTransform()[2].lerp(getLocalTransform()[2],
													 other,
												     getSpeed());

	setPosition(lerpedPosition.x, lerpedPosition.y);
}

float const Enemy::getHealth()
{
	return m_health;
}

float const Enemy::getAttack()
{
	return m_attackDamage;
}

bool const Enemy::takeDamage(float damage)
{
	m_health -= damage;
	if (m_health <= 0) {
		m_health = 0;
		return true;
	}
	return false;
}
