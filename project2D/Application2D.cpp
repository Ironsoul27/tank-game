#include "Application2D.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <sstream>
#include <random>
#include <iostream>
#include <string>
#include <Bits.h>

#include "EntityManager.h"
#include "BulletManager.h"
#include "BunnyEnemy.h"
#include "GameManager.h"
#include "EntityText.h"
#include "WaveManager.h"
#include "DefenseTurrets.h"
#include "DataManagers.h"
#include "ResourceManager.h"
#include "ConsoleText.h"
#include "Bullets.h"
#include "Tank.h"


using namespace std;

Application2D::Application2D() {
}

Application2D::~Application2D() {

}

bool Application2D::startup() {

	srand(time(NULL));
	
	m_2dRenderer = new aie::Renderer2D();

	m_textBasis = 0.0;
	m_bunnyHealth = 100;
	m_bunnySpeed = .01f;
	m_tankSpeed = 500.0f;

	// DISCLAIMER! Much of the neccessary components have yet to be included and some things where rushed just to get them working, however I wanted to submit it for assessment of the neccessary components.

	// COMMENTS
	// Interesting little tidbit, when running the application from AIE on the i7 6700's, and when using 2 seperate loops to iterate through the large set of bunnies instead of a single loop, I managed to get far superior performance than without 2 loops
	// This allowed me to have over 20,000 bunnies accompanied with over 150 bullets comparing themselves to every bunny without a single dip in frame rate 0_0
	// However once I got home and ran the application on my 8700k, which is technically superior to the 6700  in every aspect, cache, cores you name it, managed only 50 bunnies before achieving frame rate dips
	// The entire idea stemmed from a report on Stack Overflow showing the benefits of dual loop systems, however these results certaintly perplex me :\

	// UPDATE: Now running it on both computers I get very similar performance to my 8700k, must have been something that was changed in the interim

	// Rather unhappy with how this all came together, even though I mangaged to get the majority of the stuff 'working', for instance the dataManager was supposed to hold more data than it currently does, along with the EntityManager which holds entities for access
	// Across the project. These classes would be derived from and allow access to all the neccessary data inside them if required, otherwhise the pointers to the classes result in null, taking up negligible space. It was the middle ground for linking data across files. 
	// However this approach is ripe for header incursion hell which is what I had to battle with for a long time. The current solution is very patchworky and clunky with many similar classes being seperated, but it's all managed to come together in the end.
	// My intention was to have a global solution with the flexibility akin to how smart/shared_pointers would work, however since I've only just recently touched on them I was unsure on some of the neccessary implementation.

	// UPDATE: All of the header issues have been fixed, everything is in it's proper location now :D

	// Controls
	// Shoot : Space
	// Move : WASD
	// Turret : LR Arrow keys


	// Stretch Goals

	// Particle system from bunny when caught, also sparks from barrel when shooting
	// Text above bunnies at random intervals, (unless caught maybe)  CHECK
	// Bunny minimap showing nearby bunnies  CHECK ?
	// Place object on screen based on mouse position, use either camera position or tank position (maybe with an offset location for reference) and add the mouse xy position to get position relative to the camera/tank in worldspace  CHECK


	// TODO

	// Bug in release build where player spawns ontop of base
	// Place debug behaviour into seperate clasS for flexibility and accessability across files
	// Fix Wave Spawn Locations
	// If possible define or typedef large lines of code for faster readability ie. most manager calls
	// Fix bunny speedup as waves increase (DONE) 
	// Change bunnies wave behaviour to track towards home base at constant rate (DONE) 
	// Delete temporary objects on win/lose screens
	// Change planes deletion to after plane has tracked to coord ourside of screenspace instead of after timer has expired
	// Optomisations in WaveManager/Events
	// Macros where applicable
	// Instead of reaching through multiple functions from managers and taking time to add and remove from the stack, have local class pointers which will be assigned to our vector containers, making the access far quicker (Small Improvement)

	m_Resource_Manager = new ResourceManager();
	m_ConsoleText = new ConsoleText();
	m_Bullet_Manager = new BulletManager();
	m_GameState = new GameManager();
	m_Entity_Manager = new EntityManager();
	m_WaveEvent = new WaveEvent();
	m_Wave_Manager = new WaveManager(10); // pass through max number of waves
	m_EntityText = new EntityText();

	// ALL HIGHLY UNNECESSARY, However I couldn't get an automatic/global version working so best I could do was assign all the appropriate managers at startup time, which can get really finicky
	// All of them are pointing to the same data managers, but requires cumbersome allocating at startup for multiple classes
	
	m_Resource_Manager->StartUp();
	m_Bullet_Manager->Startup();

	m_Bullet_Manager->AddApplication(this);
	m_ScriptedEvents.AddApplication(this);
	m_ConsoleText->AddApplication(this);



	m_Entity_Manager->AddResourceManager(m_Resource_Manager);
	m_Bullet_Manager->AddResourceManager(m_Resource_Manager);
	m_ScriptedEvents.AddResourceManager(m_Resource_Manager);
	m_ConsoleText->AddResourceManager(m_Resource_Manager);
	m_WaveEvent->AddResourceManager(m_Resource_Manager);

	m_ScriptedEvents.AddGameState(m_GameState);
	m_Bullet_Manager->AddGameState(m_GameState);
	m_Wave_Manager->AddGameState(m_GameState);
	m_WaveEvent->AddGameState(m_GameState);


	m_Entity_Manager->GetPlayer().AddBulletManager(m_Bullet_Manager);

	m_WaveEvent->AddEntityManager(m_Entity_Manager);
	m_ScriptedEvents.AddEntityManager(m_Entity_Manager);
	m_Bullet_Manager->AddEntityManager(m_Entity_Manager);
	m_Wave_Manager->AddEntityManager(m_Entity_Manager);

	m_ScriptedEvents.AddConsoleText(m_ConsoleText);

	m_WaveEvent->AddWaveManager(m_Wave_Manager);
	m_WaveEvent->AddConsoleText(m_ConsoleText);
	m_WaveEvent->AddApplication(this);
	m_WaveEvent->AddBulletManager(m_Bullet_Manager);

	m_ConsoleText->AddEntityText(m_EntityText);

	m_Entity_Manager->GetPlayer().setup(m_Resource_Manager->GetTexture("TankTex"), m_Resource_Manager->GetTexture("TurretTex"), m_tankSpeed);
	m_Entity_Manager->GetPlayer().setPosition(getWindowWidth() / 2.f, getWindowHeight() / 2.f);


	m_Bullet_Manager->GetBullets()->reserve(150);

	if (ENEMYCOUNT > 0) {
		m_Entity_Manager->GetBunnies().reserve(ENEMYCOUNT);

		for (size_t i = 0; i < ENEMYCOUNT; i++) {
			BunnyEnemy* m_bunny = new BunnyEnemy(m_bunnyHealth, 100.0f, m_bunnySpeed);
			m_bunny->setup(m_Resource_Manager->GetTexture("EnemyTex"), BUNNYSCALE, 300, BUNNYSPAWNOFFSET, getWindowWidth(), getWindowHeight());
			m_Entity_Manager->GetBunnies().push_back(m_bunny);
		}
	}
	

	// Jump to game states (DEBUG)

	//m_GameState->setGameState(HomeDefence);
	//m_GameState->setHD_State(Transition);
	//m_GameState->setHD_State(Waves);



	return true;
}


void Application2D::shutdown() {

	for (size_t i = 0; i < m_Bullet_Manager->GetBullets()->size(); ++i) {
		delete m_Bullet_Manager->GetBullets()->at(i);
	}
	for (auto m_bunny : m_Entity_Manager->GetBunnies()) {
		delete m_bunny;
	}
	for (auto turret : m_Entity_Manager->GetTurrets()) {
		delete turret;
	}

	m_Resource_Manager->ShutDown();
	delete m_2dRenderer;
	delete m_Resource_Manager;
	delete m_ConsoleText;
	delete m_Bullet_Manager;
	delete m_GameState;
	delete m_Entity_Manager;
	delete m_WaveEvent;
	delete m_Wave_Manager;
	delete m_EntityText;
	
}


void Application2D::update(float deltaTime) {

	aie::Input* input = aie::Input::getInstance();


	switch (m_GameState->getGameState()) {

		case StartGame: {

			m_Entity_Manager->GetPlayer().update(deltaTime);

			// UpdateBunnies
			if (m_Entity_Manager->GetBunnies().size() != 0) {
				for (auto m_bunny : m_Entity_Manager->GetBunnies()) {
					m_bunny->onUpdate(deltaTime, m_Entity_Manager->GetPlayer().getGlobalTransform()[2], m_BunnyTexts); // Update position and update bunnies talking text if player is close
				}
			}

			// UpdateBullets
			if (m_Bullet_Manager->GetBullets()->size() != 0) {
				m_Bullet_Manager->UpdateBullets(deltaTime);
				updateDebugText(numBullets, e_NumBullets, nullptr);
			}
			updateDebugText(numEnemies, e_NumEnemies, nullptr);

			// Player's Bunny Radar
			if (m_Entity_Manager->GetBunnies().size() != 0)
				m_Entity_Manager->GetPlayer().UpdateRadar(deltaTime, m_Entity_Manager->GetBunnies());


			// Startgame Exit
			if (m_Entity_Manager->GetCaughtBunnies() == m_Entity_Manager->GetBunnies().size()  &&  m_Entity_Manager->GetBunnies().size() != 0) {

				for (size_t i = 0; i < m_Bullet_Manager->GetBullets()->size(); ++i) {
					delete m_Bullet_Manager->GetBullets()->at(i);
				}
				m_Bullet_Manager->GetBullets()->clear();
				m_GameState->setGameState(ScriptedEvents);
			}
			break;
		}

		// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
		//						    EVENTS
		// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

		case ScriptedEvents: {

			if (m_ScriptedEvents.Update(deltaTime, m_Entity_Manager->GetPlayer(), m_Entity_Manager->GetBunnies(), input))
				m_GameState->setGameState(HomeDefence);
			break;
		}	

		case HomeDefence: {
			m_WaveEvent->Update(deltaTime, input);

			if (m_GameState->getHD_State() == Waves) {   

				if (m_Bullet_Manager->GetBullets()->size() != 0) {  // Update Bullets
					m_Bullet_Manager->UpdateBullets(deltaTime);
					updateDebugText(numBullets, e_NumBullets, nullptr);
				}
				updateDebugText(numEnemies, e_NumEnemies, nullptr);

				if (m_Entity_Manager->GetBunnies().size() != 0)  // Update Radar
					m_Entity_Manager->GetPlayer().UpdateRadar(deltaTime, m_Entity_Manager->GetBunnies());
			}
			break;
		}
	}


	// Camera Position

	float camPosX;
	float camPosY;
	m_2dRenderer->getCameraPos(camPosX, camPosY);

	camPosX = m_Entity_Manager->GetPlayer().getGlobalTransform()[2].x - (getWindowWidth() / 2.f);
	camPosY = m_Entity_Manager->GetPlayer().getGlobalTransform()[2].y - (getWindowHeight() / 2.f);

	m_2dRenderer->setCameraPos(camPosX, camPosY);

	updateDebugText(mouseXY, e_MouseXY, input);

	

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE)) {
		quit();
	}
}




void Application2D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	m_Entity_Manager->GetPlayer().draw(m_2dRenderer);

	for (size_t i = 0; i < m_Bullet_Manager->GetBullets()->size(); ++i) {
		m_Bullet_Manager->GetBullets()->at(i)->draw(m_2dRenderer);
	}

	for (auto m_bunny : m_Entity_Manager->GetBunnies()) {
		m_bunny->draw(m_2dRenderer);

		if ((m_GameState->getGameState() == StartGame || m_GameState->getHD_State() == Waves) && m_Entity_Manager->GetBunnies().size() != 0) {
			m_2dRenderer->drawLine(m_Entity_Manager->GetPlayer().getLocalTransform()[2].x, m_Entity_Manager->GetPlayer().getLocalTransform()[2].y, m_Entity_Manager->GetPlayer().getLocalTransform()[2].x + m_Entity_Manager->GetPlayer().GetRadarFacing().x, m_Entity_Manager->GetPlayer().getLocalTransform()[2].y + m_Entity_Manager->GetPlayer().GetRadarFacing().y); // Bunny radar

			if (!m_bunny->isCaught() && m_bunny->isMessageDisplayed())
				m_2dRenderer->drawText(m_Resource_Manager->GetFont("ConsoleFont"), m_bunny->getText().c_str(), m_bunny->getLocalTransform()[2].x - 290, m_bunny->getLocalTransform()[2].y + 110); // Random bunny talk
		}
	}
		
	

	// UI Elements
	char fps[32];
	float tank_XAxis = m_Entity_Manager->GetPlayer().getLocalTransform()[2].x + getWindowWidth() / -2.0f;
	float tank_YAxis = m_Entity_Manager->GetPlayer().getLocalTransform()[2].y + getWindowHeight() / 2.0f;
	unsigned int windowHeight = getWindowHeight();
	unsigned int windowWidth = getWindowWidth();


	sprintf_s(fps, 32, "FPS: %i", getFPS());

	m_2dRenderer->drawText(m_Resource_Manager->GetFont("DefaultFont"), fps, tank_XAxis, tank_YAxis + (m_textBasis -= TEXTOFFSET));
	m_2dRenderer->drawText(m_Resource_Manager->GetFont("DefaultFont"), "Press ESC to quit!", tank_XAxis, tank_YAxis + (m_textBasis -= TEXTOFFSET));

	switch (m_GameState->getGameState()) {

		case StartGame: {
			// Call debug draw from debug class

			if (m_Bullet_Manager->GetBullets()->size() != 0) {
				m_2dRenderer->drawText(m_Resource_Manager->GetFont("DefaultFont"), b_Axis.c_str(), tank_XAxis, tank_YAxis + (m_textBasis -= TEXTOFFSET));
				m_2dRenderer->drawText(m_Resource_Manager->GetFont("DefaultFont"), bulletTimer.c_str(), tank_XAxis, tank_YAxis + (m_textBasis -= TEXTOFFSET));
				m_2dRenderer->drawText(m_Resource_Manager->GetFont("DefaultFont"), numBullets.c_str(), tank_XAxis, tank_YAxis + (m_textBasis -= TEXTOFFSET));
			}
			if (m_Entity_Manager->GetCaughtBunnies() != m_Entity_Manager->GetBunnies().size())
				m_2dRenderer->drawText(m_Resource_Manager->GetFont("DefaultFont"), numEnemies.c_str(), tank_XAxis, tank_YAxis + (m_textBasis -= TEXTOFFSET));

			m_2dRenderer->drawText(m_Resource_Manager->GetFont("DefaultFont"), mouseXY.c_str(), tank_XAxis, tank_YAxis + (m_textBasis -= TEXTOFFSET));

			break;
		}

		case ScriptedEvents: {
			m_ScriptedEvents.Draw(m_2dRenderer);
			break;
		}
							 
		case HomeDefence: {
			m_WaveEvent->Draw(m_2dRenderer);
			break;
		}
	}

	m_textBasis = 0.0;

	// done drawing sprites
	m_2dRenderer->end();
}



void const Application2D::updateDebugText(string& text, unsigned int check, aie::Input* input = nullptr) { 
	std::stringstream s; 

	switch (check) {
		case e_BulletTimer: {
			s << "Bullet Timer:  " << m_Bullet_Manager->GetBullets()->at(m_Bullet_Manager->GetBullets()->size() - 1)->getTimer();
			break;
		}
		case e_BulletAxis: {
			s << "Bullet Axis:  " << m_Bullet_Manager->GetBullets()->at(m_Bullet_Manager->GetBullets()->size() - 1)->getLocalTransform()[2].x << "  " << m_Bullet_Manager->GetBullets()->at(m_Bullet_Manager->GetBullets()->size() - 1)->getLocalTransform()[2].y;
			break;
		}
		case e_NumBullets: {
			s << "Number Of Bullets:  " << m_Bullet_Manager->GetBullets()->size();
			break;
		}
		case e_NumEnemies: {
			s << "Number Of Adorable Bunnies Caught:  " << m_Entity_Manager->GetCaughtBunnies() << " / " << m_Entity_Manager->GetBunnies().size();
			break;
		}
		case e_MouseXY: {
			s << "Mouse_XY:  " << input->getMouseX() << "  " << input->getMouseY();
			break;
		}
		default: {
			throw "ERROR, Cannot validate array type";
			return;
		}
	}
	
	text = s.str();
}



//void const Application2D::SpawnToSSCoordinates(aie::Input* input) //  click and drag maybe?
//{
//	Vector3 offSetPos = m_Entity_Manager->GetPlayer().getGlobalTransform()[2];
//	offSetPos.x -= (getWindowWidth() / 2);
//	offSetPos.y -= (getWindowHeight() / 2);
//
//	offSetPos.x += input->getMouseX();
//	offSetPos.y += input->getMouseY();
//
//	DefenseTurrets* turret = new DefenseTurrets();
//	turret->AddBulletManager(m_Bullet_Manager);
//	turret->AddResourceManager(m_Resource_Manager);
//	turret->AddEntityManager(m_Entity_Manager);
//	turret->Setup(m_Resource_Manager->GetTexture("TurretTex"), 1.2f, offSetPos.x, offSetPos.y);
//	
//	m_Entity_Manager->GetTurrets().push_back(turret);
//}