#include "BulletManager.h"
#include "ResourceManager.h"
#include "BunnyEnemy.h"
#include "EntityManager.h"
#include "GameManager.h"
#include "Bullets.h"


BulletManager::BulletManager()
{
}

BulletManager::~BulletManager()
{
}

void const BulletManager::Startup()
{
	m_Bullets = std::make_shared<vector<Bullets*>>();
}

std::shared_ptr<vector<Bullets*>> BulletManager::GetBullets()
{
	return m_Bullets;
}

Bullets* const BulletManager::ConstructBullet(float speed, float b_timer)
{
	Bullets* m_Bullet = new Bullets(m_ResourceManager->GetTexture("BulletTex"), speed, b_timer);
	return m_Bullet;
}


void const BulletManager::UpdateBullets(float deltaTime)
{
	for (size_t i = 0; i < m_Bullets->size(); ++i) {
		m_Bullets->at(i)->update(deltaTime);		// update position

		if (m_Bullets->at(i)->getTimer() <= 0) {
			delete m_Bullets->at(i);
			m_Bullets->erase(m_Bullets->begin() + i);
			continue;
		}

		// Loops over half each to significantly improve performance ( 'Potentially', see comments at top )
		size_t bunnyArraySize = m_EntityManager->GetBunnies().size();
		bool m_bunnyFound = false;

		for (size_t j = 0; j < (bunnyArraySize / 2); ++j) {
			if (CheckForBunny(j, i)) {
				m_bunnyFound = true;
				break;
			}
		}
		if (!m_bunnyFound) {
			for (size_t j = bunnyArraySize / 2; j < bunnyArraySize; ++j) {
				if (CheckForBunny(j, i))
					break;
			}
		}
	}
}


bool const BulletManager::CheckForBunny(size_t j, size_t i) // bunny, bullet
{
	if (!m_EntityManager->GetBunnies()[j]->isCaught() && m_EntityManager->GetBunnies()[j]->getLocalTransform()[2].distanceSqr(m_Bullets->at(i)->getLocalTransform()[2]) < 9000.0f) {
		delete m_Bullets->at(i);
		m_Bullets->erase(m_Bullets->begin() + i);

		if (m_GameManager->getHD_State() == Waves) {
			delete m_EntityManager->GetBunnies()[j];
			m_EntityManager->GetBunnies().erase(m_EntityManager->GetBunnies().begin() + j);
		}
		else {
			m_EntityManager->IncrementCaughtBunnies();
			m_EntityManager->GetBunnies()[j]->setCaught();
		}
		return true;
	}
	return false;
}