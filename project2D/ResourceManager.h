#pragma once
#include "Renderer2D.h"
#include "Texture.h"
#include "Font.h"
#include <memory>
#include <unordered_map>
#include <string>
#include <vector>

using std::string;
using std::vector;

typedef std::tuple<string, string, bool> TextTuple;

typedef std::unordered_map<string, std::shared_ptr<aie::Texture>> TextureMap;
typedef std::unordered_map<string, aie::Font*> FontMap;
typedef std::unordered_map<string, vector <TextTuple>> BunnyTexts;

class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();

	void const StartUp();
	void const ShutDown();

	std::shared_ptr<aie::Texture>& GetTexture(string key);
	aie::Font* GetFont(string key);
	vector<TextTuple> GetTexts(string key);

private:
	

	// Fonts
	aie::Font* m_Font;
	aie::Font* m_ConsoleFont;

	// Textures
	std::shared_ptr<aie::Texture>		m_enemy_tex;
	std::shared_ptr<aie::Texture>		m_plane_tex;
	std::shared_ptr<aie::Texture>		m_tank_tex;
	std::shared_ptr<aie::Texture>		m_turret_tex;
	std::shared_ptr<aie::Texture>	    m_carrotBullet_tex;

	// Resource HashMaps
	TextureMap m_Textures; 
	FontMap m_Fonts;
	BunnyTexts m_BunnyText;
};

