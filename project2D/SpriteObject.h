#pragma once
#include "SceneObject.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "Renderer2D.h"
#include <memory>

using namespace aie;

class SpriteObject : public SceneObject 
{
public:

	SpriteObject();
	SpriteObject(const char* filename) { load(filename); }
	virtual ~SpriteObject() {}

	bool load(const char* filename);
	virtual void onDraw(aie::Renderer2D* renderer);
	bool setTexture(std::shared_ptr<aie::Texture>& texture);
	Vector2& getVelocity();
	void setVelocity(const float scalar);

protected:

	std::shared_ptr<aie::Texture> m_texture = nullptr;

	Vector2 Velocity;

};
