#include "Application2D.h"
#include <stdlib.h>
#include <crtdbg.h>

#define _CRTDBG_MAP_ALLOC

int main() {
	
	// allocation
	auto app = new Application2D();

	// initialise and loop
	app->run("AIE", 1920, 1080, true);

	// deallocation
	delete app;

	_CrtDumpMemoryLeaks();

	return 0;
}