#include "GameManager.h"



GameManager::GameManager()
{
}


GameManager::~GameManager()
{
}

int const GameManager::getGameState() const
{
	return gameState;
}

void const GameManager::setGameState(int gameState)
{
	this->gameState = gameState;
}

int const GameManager::getSE_State() const
{
	return SE_State;
}

void const GameManager::setSE_State(int SE_State)
{
	this->SE_State = SE_State;
}

int const GameManager::getHD_State() const
{
	return HD_State;
}

void const GameManager::setHD_State(int HD_State)
{
	this->HD_State = HD_State;
}
