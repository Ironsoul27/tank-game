#include "BunnyEnemy.h"
#include "SpriteObject.h"
#include "GameManager.h"



BunnyEnemy::BunnyEnemy()
{
	m_health = 100;
	m_attackDamage = 10;
	m_speed = .009f;
	setVelocity(300);
	srand(0);
}

BunnyEnemy::BunnyEnemy(float m_health, float attackDamage, float m_speed)
{
	this->m_health = m_health;
	this->m_attackDamage = m_attackDamage;
	this->m_speed = m_speed;
}


BunnyEnemy::~BunnyEnemy()
{
}


void const BunnyEnemy::setup(std::shared_ptr<aie::Texture>& texture, float scale, float velocity, unsigned int spawnOffset, unsigned int windowWidth, unsigned int windowHeight)
{
	setTexture(texture);
	setScale(scale, scale);
	setVelocity(velocity);
	setPosition((windowWidth / 2.f) - rand() % spawnOffset, (windowHeight / 2.f) - rand() % spawnOffset);
}

void const BunnyEnemy::setup(std::shared_ptr<aie::Texture>& texture, float scale, float velocity, float posX, float posY)
{
	setTexture(texture);
	setScale(scale, scale);
	setVelocity(velocity);
	setPosition(posX, posY);
}

void BunnyEnemy::onUpdate(float deltaTime, Vector3 target_pos, vector<string>& m_bunnyTexts) // target position, or get spriteObject position
{
	
	if (m_caughtStatus) {
		auto lerpedPosition = getLocalTransform()[2].lerp(getLocalTransform()[2],
			target_pos,
			m_speed);

		setPosition(lerpedPosition.x, lerpedPosition.y);
	}
	else if (!m_messageDisplayed && getLocalTransform()[2].distanceSqr(target_pos) < 300000.0f) {
		m_bunnyText = assignRandText(m_bunnyTexts);
		m_messageDisplayed = true;
	}
	else if (m_messageDisplayed && getLocalTransform()[2].distanceSqr(target_pos) > 300000.0f) {
		m_bunnyText.clear();
		m_messageDisplayed = false;
	}
}


bool const BunnyEnemy::isCaught() const
{
	return(m_caughtStatus);
}

void const BunnyEnemy::setCaught()
{
	m_caughtStatus = !m_caughtStatus;
}

Vector3& BunnyEnemy::getCirclePosition()
{
	return m_CirclePosition;
}

bool const BunnyEnemy::isMessageDisplayed() const
{
	return m_messageDisplayed;
}

bool const BunnyEnemy::isCloseToTarget() const
{
	return m_closeToTarget;
}

void const BunnyEnemy::setIsClose()
{
	m_closeToTarget = true;
}

string const BunnyEnemy::assignRandText(vector<string>& m_bunnyTexts)
{
	return m_bunnyTexts[rand() % m_bunnyTexts.size()];
}

string& BunnyEnemy::getText()
{
	return m_bunnyText;
}