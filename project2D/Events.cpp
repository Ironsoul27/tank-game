#include "Events.h"
#include "SpriteObject.h"
#include "Input.h"
#include "ConsoleText.h"
#include "WaveManager.h"
#include "GameManager.h"
#include "Application.h"
#include "ResourceManager.h"
#include "EntityManager.h"
#include "BulletManager.h"
#include "DefenseTurrets.h"
#include "BunnyEnemy.h"
#include "Tank.h"

ScriptedEvents::ScriptedEvents()
{
	m_Plane.setScale(.15f, .15f);
	m_Plane.setVelocity(3200);
}


ScriptedEvents::~ScriptedEvents()
{
}

bool const ScriptedEvents::Update(float deltaTime, Tank& m_tank, vector<BunnyEnemy*>& m_Bunnies, aie::Input* input)
{
	switch (m_GameManager->getSE_State()) {

		case BunniesPositions: {
			m_LeadBunny = m_Bunnies[0];

			for (auto m_bunny : m_Bunnies) {
				FindBunniesPositions(m_bunny, m_tank); 
			}

			m_GameManager->setSE_State(BunniesMoving);
			break;
		}

		case BunniesMoving: {
			for (size_t i = 1; i < m_Bunnies.size(); ++i)
				m_Bunnies[i]->lerpPosition(m_Bunnies[i]->getCirclePosition());

			if (m_Bunnies[1]->getLocalTransform()[2].withinRange(m_Bunnies[1]->getCirclePosition(), 0.6f))
				m_GameManager->setSE_State(LeaderMoving);
			break;
		}


		case LeaderMoving: {
			m_LeadBunny->lerpPosition(m_LeadBunny->getCirclePosition());

			if (m_LeadBunny->getLocalTransform()[2].withinRange(m_LeadBunny->getCirclePosition(), 0.7f))
				m_GameManager->setSE_State(Introductions);
			break;
		}


		case Introductions: {
			if (m_ConsoleText->UpdateConsoleText(m_ResourceManager->GetTexts("Text_1"), deltaTime))
				m_GameManager->setSE_State(ShootAttempt);
			break;
		}

		case ShootAttempt: {
			if (!m_didPlayerShoot && input->wasKeyPressed(aie::INPUT_KEY_SPACE)) {
				m_didPlayerShoot = true;
			}
			if (m_didPlayerShoot)
				if (m_ConsoleText->UpdateConsoleText(m_ResourceManager->GetTexts("Text_2"), deltaTime))
					m_GameManager->setSE_State(PlaneFlyby);

			break;
		}

		case PlaneFlyby: {
			if (!m_hasPlaneSpawned) {
				m_Plane.setTexture(m_ResourceManager->GetTexture("PlaneTex"));
				m_Plane.setPosition(m_tank.getLocalTransform()[2].x + (m_App->getWindowWidth() / 2.0f) - (m_App->getWindowWidth() + 300), m_tank.getLocalTransform()[2].y + (m_App->getWindowHeight() / 2.0f) - 140);
				m_hasPlaneSpawned = true;
			}
			m_planeTimer -= deltaTime;

			auto facing = m_Plane.getLocalTransform()[0] * deltaTime * m_Plane.getVelocity();
			m_Plane.translate(facing.x, facing.y);

			if (m_planeTimer <= 0)
				m_GameManager->setSE_State(Ending);

			break;
		}

		case Ending: {
			if (m_ConsoleText->UpdateConsoleText(m_ResourceManager->GetTexts("Text_3"), deltaTime)) {
				m_GameManager->setGameState(Waves);
				m_GameManager->setHD_State(Transition);
				return true;
			}
			break;
		}
	}
	return false;
}


void const ScriptedEvents::Draw(aie::Renderer2D * m_2dRenderer)
{
	if (m_GameManager->getSE_State() == PlaneFlyby) {
		m_Plane.draw(m_2dRenderer);
		m_2dRenderer->setRenderColour(255.f, 255.f, 255.f);
		m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), PlaneText.c_str(), m_Plane.getLocalTransform()[2].x - 500, m_Plane.getLocalTransform()[2].y - 20);
	}

	m_ConsoleText->Draw(m_2dRenderer, m_EntityManager->GetPlayer()); // Draw Console
}


SpriteObject & ScriptedEvents::GetPlane()
{
	return m_Plane;
}


void const ScriptedEvents::FindBunniesPositions(BunnyEnemy * m_bunny, Tank& m_tank)
{
	Matrix3 m = m_tank.m_Turret.getGlobalTransform();

	if (m_bunny == m_LeadBunny) {
		auto facing = m[1];
		m.translate(facing.x * LEADER_CIRCLEOFFSET, facing.y * LEADER_CIRCLEOFFSET);
	}
	else {
		m.rotateZ(rand() / 2.0f);
		auto facing = m[1];
		m.translate(facing.x * BUNNY_CIRCLEOFFSET, facing.y * BUNNY_CIRCLEOFFSET);
	}

	m_bunny->getCirclePosition() = m[2];
}


void const ScriptedEvents::AddConsoleText(ConsoleText* CT)
{
	m_ConsoleText = CT;
}





WaveEvent::WaveEvent()
{
}

WaveEvent::~WaveEvent()
{
}

void const WaveEvent::Update(float deltaTime, aie::Input* input)
{

	for (DefenseTurrets* turret : m_EntityManager->GetTurrets()) {
		turret->OnUpdate(deltaTime);
	}

	switch (m_GameManager->getHD_State()) {
		case Transition: {
			if (SceneTransition(deltaTime)) {
				m_GameManager->setHD_State(Intermission);
			}
			break;
		}

		case Intermission: {
			if (m_ConsoleText->UpdateConsoleText(m_ResourceManager->GetTexts("Text_4"), deltaTime))
				m_GameManager->setHD_State(Waves);
			break;
		}

		case Waves: {

			if (input->wasKeyPressed(INPUT_KEY_G)) // Spawn Turret
				SpawnToSSCoordinates(input);

			m_EntityManager->GetPlayer().update(deltaTime); // Update Player
			UpdateBunnies(deltaTime); // Update Bunnies

			if (m_Wave_Manager->Startup(m_EntityManager->GetHomeBase(), 5)) { // If new wave, recalcualte spawn locations and check win conditions // change to check after all bunnies are destroyed. Method to check if the wave we lost at is the final one to ensure we don't win
				if (m_Wave_Manager->HasPlayerWon()) {
					m_GameManager->setHD_State(Win);
				}	
				else if (m_EntityManager->GetHomeBase().getHealth() <= 0) {
					m_GameManager->setHD_State(Lose);
				}
			}

			if (m_EntityManager->GetBunnies().size() == 0) { // Spawn Next Wave
				m_Wave_Manager->SpawnEnemy();
				m_Wave_Manager->SetNextWave();
			}
			break;
		}

		case Win: {

			break;
		}

		case Lose: {

			break;
		}
	}

}

void const WaveEvent::Draw(aie::Renderer2D * m_2dRenderer)
{
	if (m_GameManager->getHD_State() != Transition) {
		m_EntityManager->GetHomeBase().draw(m_2dRenderer);
		m_2dRenderer->drawText(m_ResourceManager->GetFont("DefaultFont"), "HOME-BAZE", m_EntityManager->GetHomeBase().m_HomeBase.getLocalTransform()[2].x - 70, m_EntityManager->GetHomeBase().m_HomeBase.getLocalTransform()[2].y + 170);
		m_2dRenderer->drawText(m_ResourceManager->GetFont("DefaultFont"), "NO GIRLZ ALLOWED!", m_EntityManager->GetHomeBase().m_HomeBase.getLocalTransform()[2].x - 140, m_EntityManager->GetHomeBase().m_HomeBase.getLocalTransform()[2].y + 120);
		m_2dRenderer->drawBox(m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2].x, m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2].y + 240, fmod(m_EntityManager->GetHomeBase().getHealth(), 500), 12);
		m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), "Base Health", m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2].x - 100, m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2].y + 260);
	}

	for (auto turret : m_EntityManager->GetTurrets()) {

		turret->m_Turret.draw(m_2dRenderer);
		m_2dRenderer->setRenderColour(1, 1, 1, .02f);
		m_2dRenderer->drawCircle(turret->m_Turret.getGlobalTransform()[2].x, turret->m_Turret.getGlobalTransform()[2].y, 500, 0);
		m_2dRenderer->setRenderColour(1, 1, 1, 1);
	}

	switch (m_GameManager->getHD_State()) {
		case Transition: {
			m_2dRenderer->setRenderColour(0, 0, 0, m_AlphaNum);
			m_2dRenderer->drawBox(m_EntityManager->GetPlayer().getGlobalTransform()[2].x, m_EntityManager->GetPlayer().getGlobalTransform()[2].y, m_App->getWindowWidth(), m_App->getWindowHeight());
			break;
		}

		case Intermission: {
			m_ConsoleText->Draw(m_2dRenderer, m_EntityManager->GetPlayer());
			break;
		}
		case Waves: {
			
			m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), "Press 'G' to place turrets", m_EntityManager->GetPlayer().getGlobalTransform()[2].x - (m_App->getWindowWidth() / 2), m_EntityManager->GetPlayer().getGlobalTransform()[2].y - (m_App->getWindowHeight() / 2) + 20);
			m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), m_Wave_Manager->GetCurremtWaveText().c_str(), m_EntityManager->GetPlayer().getGlobalTransform()[2].x + (m_App->getWindowWidth() / 2 - 300), m_EntityManager->GetPlayer().getGlobalTransform()[2].y - (m_App->getWindowHeight() / 2) + 20);

			for (auto bunny : m_EntityManager->GetBunnies())
			{
				if (bunny->isCloseToTarget() == true)
					m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), "FOR THE FLUFFYLAND!", bunny->getGlobalTransform()[2].x - 200, bunny->getGlobalTransform()[2].y + 110);
			}
			break;
		}
		case Win: {
			m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), "YOU WON! YAY! 'YEP IT'S STILL VERY UNFINISHED :D'", m_EntityManager->GetPlayer().getGlobalTransform()[2].x, m_EntityManager->GetPlayer().getGlobalTransform()[2].y - (m_App->getWindowHeight() / 2) + 20);
			break;
		}
		case Lose: {
			m_2dRenderer->drawText(m_ResourceManager->GetFont("ConsoleFont"), "YOU LOST OH NO! 'YEP IT'S STILL VERY UNFINISHED :D'", m_EntityManager->GetPlayer().getGlobalTransform()[2].x, m_EntityManager->GetPlayer().getGlobalTransform()[2].y - 400);
			break;
		}
	}
}

bool const WaveEvent::SceneTransition(float deltaTime)
{
	if (m_AlphaNum <= 1 && !m_flipDir) {
		m_AlphaNum += (deltaTime / m_FadeRate);

		return false;
	}
	else if (!m_flipDir){
		m_TransitionTimer -= deltaTime;

		if (m_TransitionTimer <= 0) { // once the screen has been completely blocked, call functions to delete / reset objects
			for (int i = m_EntityManager->GetBunnies().size() - 1; i >= 0; --i) {
				delete m_EntityManager->GetBunnies().at(i);
				m_EntityManager->GetBunnies().erase(m_EntityManager->GetBunnies().begin() + i);
			}

			m_EntityManager->GetPlayer().setPosition(m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2].x, m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2].y - (m_App->getWindowHeight() / 5));
			m_EntityManager->GetPlayer().m_Turret.setRotate(6.28319);
			m_EntityManager->GetPlayer().setRotate(6.28319);
			m_flipDir = true;
		}
		return false;
	}
	if (m_flipDir) {
		m_AlphaNum -= (deltaTime / m_FadeRate);
		if (m_AlphaNum <= 0) {
			m_AlphaNum = 0;
				return true;
		}
		return false;
	}
	return false;
}


void const WaveEvent::AddWaveManager(WaveManager * WM)
{
	m_Wave_Manager = WM;
}

void const WaveEvent::AddConsoleText(ConsoleText * CT)
{
	m_ConsoleText = CT;
}


void const WaveEvent::UpdateBunnies(float deltaTime)
{
	float seekSpeed = 340;

	for (size_t i = 0; i < m_EntityManager->GetBunnies().size(); ++i) {

		Vector3 HBPosition = m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2];		
		Vector3 BunnyPosition = m_EntityManager->GetBunnies().at(i)->getGlobalTransform()[2];		

		Vector3 dir = HBPosition - BunnyPosition;
		float magSqr = (dir.x * dir.x + dir.y * dir.y);

			float mag = sqrt(magSqr);
			m_EntityManager->GetBunnies().at(i)->getVelocity() = dir * (seekSpeed / mag) * deltaTime;

			// Set Position
			m_EntityManager->GetBunnies().at(i)->setPosition((m_EntityManager->GetBunnies().at(i)->getGlobalTransform()[2].x + m_EntityManager->GetBunnies().at(i)->getVelocity().x), (m_EntityManager->GetBunnies().at(i)->getGlobalTransform()[2].y + m_EntityManager->GetBunnies().at(i)->getVelocity().y));
	
			if (m_EntityManager->GetBunnies().at(i)->getGlobalTransform()[2].distanceSqr(m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2]) < 230000)
				m_EntityManager->GetBunnies().at(i)->setIsClose();

			// If reached home base
		if (m_EntityManager->GetBunnies().at(i)->getGlobalTransform()[2].distanceSqr(m_EntityManager->GetHomeBase().m_HomeBase.getGlobalTransform()[2]) < 17000) {
			m_EntityManager->GetHomeBase().takeDamage(10);
			delete m_EntityManager->GetBunnies().at(i);
			m_EntityManager->GetBunnies().erase(m_EntityManager->GetBunnies().begin() + i);
		}
	}
}


void const WaveEvent::SpawnToSSCoordinates(aie::Input* input) //  click and drag maybe?
{
	Vector3 offSetPos = m_EntityManager->GetPlayer().getGlobalTransform()[2];
	offSetPos.x -= (m_App->getWindowWidth() / 2);
	offSetPos.y -= (m_App->getWindowHeight() / 2);

	offSetPos.x += input->getMouseX();
	offSetPos.y += input->getMouseY();

	DefenseTurrets* turret = new DefenseTurrets();
	turret->AddBulletManager(m_BulletManager);
	turret->AddResourceManager(m_ResourceManager);
	turret->AddEntityManager(m_EntityManager);
	turret->Setup(m_ResourceManager->GetTexture("TurretTex"), 1.2f, offSetPos.x, offSetPos.y);

	m_EntityManager->GetTurrets().push_back(turret);
}
