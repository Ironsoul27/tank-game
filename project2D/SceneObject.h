#pragma once
#include <vector>
#include <assert.h>
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "Matrix3.h"
//#include <Vector2.h>
//#include <Vector3.h>

using std::vector;

class SceneObject 
{
public:
	friend class DefenseTurrets;

	SceneObject();
	virtual ~SceneObject() {
		// detach from parent
		if (m_parent != nullptr)
			  m_parent->removeChild(this);

		for (auto child : m_children) 
		      child->m_parent = nullptr;
	}

	SceneObject* getParent() const { return m_parent; }
	
	size_t childCount() const { return m_children.size(); }
	
	SceneObject* getChild(unsigned int index) const { 
		return m_children[index]; }


	void addChild(SceneObject* child) { 
		// make sure it doesn't have a parent already 
		assert(child->m_parent == nullptr); 

		// assign "this as parent 
		child->m_parent = this; 

		// add new child to collection
		m_children.push_back(child);
	}


	void removeChild(SceneObject* child) { 
		// find the child in the collection 
		auto iter = std::find(m_children.begin(),
							  m_children.end(), child);
		
		// if found, remove it 
		if (iter != m_children.end()) {
			m_children.erase(iter);
			// also unassign parent 
			child->m_parent = nullptr; }
	}


	virtual void onUpdate(float deltaTime) { }
	virtual void onDraw(aie::Renderer2D* renderer) { } // look at blank virtual functions like a form of function declaration, and is intended to be substituted in derived classes


	void update(float deltaTime) {
		// run onUpdate behaviour 
		onUpdate(deltaTime);

		// update children
		for (auto child : m_children) 
				child->update(deltaTime);
	}


	void draw(aie::Renderer2D* renderer) {
		// run onDraw behaviour
		onDraw(renderer);

		// draw children
		for (auto child : m_children) 
				child->draw(renderer);
	}

	const Matrix3& getLocalTransform() { 
		return m_localTransform; 
	}

	const void setLocalTransform(const Matrix3& other) {
		m_localTransform = other;
		updateTransform();
	}

	void const setLocalTransformFacing(Vector3& v) {
		m_localTransform[0] = v;
		updateTransform();
	}
	
	void const setLocalTransformPerpindicularFacing(Vector3& v) {
		m_localTransform[1] = v;
		updateTransform();
	}

	const Matrix3& getGlobalTransform() const {
		return m_globalTransform; 
	}

	void updateTransform() {
		if (m_parent != nullptr) 
			m_globalTransform = m_parent->m_globalTransform *
								m_localTransform;
		else 
			m_globalTransform = m_localTransform; 

		for (auto child : m_children)
			child->updateTransform(); 
	}


	void setPosition(float x, float y) {
		m_localTransform[2] = { x, y, 1 };
		updateTransform(); 
	} 
	
	void setRotate(float radians) {
		m_localTransform.setRotateZ(radians);
		updateTransform(); 
	}
	
	void setScale(float width, float height) {
		m_localTransform.setScaled(width, height, 1);
		updateTransform();
	} 
	
	void translate(float x, float y) {
		m_localTransform.translate(x, y);
		updateTransform();
	}
	
	void rotate(float radians) {
		m_localTransform.rotateZ(radians);
		updateTransform();
	}
	
	void scale(float width, float height) {
		m_localTransform.scale(width, height, 1);
		updateTransform();
	}



protected: 

	SceneObject* m_parent = nullptr;
	std::vector<SceneObject*> m_children;

	Matrix3 m_localTransform = Matrix3::identity;
	Matrix3 m_globalTransform = Matrix3::identity;
};
