#include "Turret.h"


Turret::Turret() {
	r_multiplier = 5;
}

Turret::Turret(float r_multipler) {
	this->r_multiplier = r_multipler;
}

Turret::~Turret()
{
}


float const Turret::getRotationMultiplier() {
	return r_multiplier;
}