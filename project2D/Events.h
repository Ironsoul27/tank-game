#pragma once
#include "DataManagers.h"
#include "SpriteObject.h"
#include <iostream>
#include <string>
#include <math.h>
#include <cmath>
#include "Vector2.h"

class WaveManager;
class BunnyEnemy;
class ConsoleText;
class Tank;

using std::string;


class ScriptedEvents : public DataManagers
{
public:
	ScriptedEvents();
	virtual ~ScriptedEvents();

	bool const Update(float deltaTime, Tank& m_tank, vector<BunnyEnemy*>& m_Bunnies, aie::Input* input);
	void const Draw(aie::Renderer2D* m_2dRenderer);
	SpriteObject& GetPlane();
	void const FindBunniesPositions(BunnyEnemy* m_bunny, Tank& m_tank);
	void const AddConsoleText(ConsoleText* CT);
	

private:
	typedef std::tuple<string, string, bool> TextTuple;

#define BUNNY_CIRCLEOFFSET 400
#define LEADER_CIRCLEOFFSET 200

	SpriteObject m_Plane;
	BunnyEnemy* m_LeadBunny;
	ConsoleText* m_ConsoleText;

	bool m_hasPlaneSpawned = false;
	bool m_didPlayerShoot = false;

	string PlaneText = "YOU CAN DO IT! ---- ";
	float m_planeTimer = 6;
};



class WaveEvent : public DataManagers
{
public:
	WaveEvent();
	virtual ~WaveEvent();

	void const Update(float deltaTime, aie::Input* input);
	void const Draw(aie::Renderer2D* m_2dRenderer);

	bool const SceneTransition(float deltaTime);

	void const AddWaveManager(WaveManager* WM);
	void const AddConsoleText(ConsoleText* CT);
	void const UpdateBunnies(float deltaTime);
	void const SpawnToSSCoordinates(aie::Input* input);

private:

	float m_TransitionTimer = 2;
	float m_AlphaNum = 0;
	float m_FadeRate = 2;

	bool m_flipDir = false;

	WaveManager* m_Wave_Manager = nullptr;
	ConsoleText* m_ConsoleText = nullptr;
	
};