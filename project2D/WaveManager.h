#pragma once
#include "DataManagers.h"
#include <Vector3.h>
#include <Matrix3.h>
#include <vector>
#include <utility>
#include <memory>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <string>

class HomeBase;

using std::vector;
using std::pair;
using std::string;

class WaveManager : public DataManagers
{
public:
	WaveManager();
	WaveManager(unsigned int maxWaves);
	~WaveManager();

	size_t const GetCurrentWave();
	string const GetCurremtWaveText();
	void const SetNextWave();
	void const SpawnEnemy();
	bool const SetSpawns(HomeBase& HB, size_t numSpawnLocs);
	bool const Startup(HomeBase& HB, size_t numSpawnLocs);	
	bool const HasPlayerWon();

private:

	vector<Vector3> m_SpawnLocations;

	bool  m_CalcSpawnLoc = true;

	float m_EnemySpawnRate = .4f;

	size_t m_MaxWaves = 5;
	size_t m_CurrentWave = 0;
};
