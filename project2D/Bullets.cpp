#include "Bullets.h"



Bullets::Bullets(std::shared_ptr<aie::Texture>& texture, float m_speed, float b_timer = 1)
{
	this->m_speed = m_speed;
	this->b_timer = b_timer;
	setTexture(texture);

	setVelocity(m_speed);
}


Bullets::~Bullets()
{
}

float const Bullets::getSpeed() {
	return m_speed;
}

float const Bullets::getTimer()
{
	return b_timer;
}

void Bullets::onUpdate(float deltaTime) 
{
	b_timer -= deltaTime;

	auto facing = getLocalTransform()[1] * deltaTime * getVelocity();
	translate(facing.x, facing.y);
}



