#pragma once
#include "DataManagers.h"
#include "Tank.h"
#include "HomeBase.h"
#include <vector>

class Tank;
class HomeBase;
class DefenseTurrets;
class BunnyEnemy;

using std::vector;


class EntityManager : public DataManagers
{
public:

	EntityManager();
	virtual ~EntityManager();

	Tank& GetPlayer();
	HomeBase& GetHomeBase();
	vector<DefenseTurrets*>& GetTurrets();
	size_t const GetMaxTurrets() const;

	size_t const GetCaughtBunnies() { return m_CaughtBunnies; }
	vector<BunnyEnemy*>& GetBunnies();
	BunnyEnemy* ConstructBunny(float posX, float posY);
	void const IncrementCaughtBunnies();

private:

	Tank						m_Tank;
	HomeBase					m_HomeBase;
	vector<DefenseTurrets*>		m_DefenseTurrets;
	vector<BunnyEnemy*>			m_Bunnies;

private:

	size_t m_CaughtBunnies = 0;
	size_t m_MaxNumTurrets = 5;

};