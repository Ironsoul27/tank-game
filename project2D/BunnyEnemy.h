#pragma once
#include "Enemy.h"
#include <string>
#include <random>
#include <memory>


using std::string;

class BunnyEnemy : public Enemy
{
public:
	BunnyEnemy();
	BunnyEnemy(float m_health, float attackDamage, float m_speed);
	virtual ~BunnyEnemy();

	virtual void onUpdate(float deltaTime, Vector3 tank_pos, vector<string>& m_bunnyTexts);
	void const setup(std::shared_ptr<aie::Texture>& texture, float scale, float velocity, unsigned int spawnOffset, unsigned int windowWidth, unsigned int windowHeight);
	void const setup(std::shared_ptr<aie::Texture>& texture, float scale, float velocity, float posX, float posY);

	Vector3& getVelocity() { return m_Velocity; }

	bool const isCaught() const;
	void const setCaught();
	Vector3& getCirclePosition();
	bool const isMessageDisplayed() const;
	bool const isCloseToTarget() const;
	void const setIsClose();
	string& getText();


private:
	string const assignRandText(vector<string>& m_bunnyTexts);

	enum GameStates {
		StartGame,
		ScriptedEvents,
		HomeDefence
	};

	enum HBDefence {
		Default,
		Transition,
		Intermission,
		Win,
		Lose,
		Waves
	};

	Vector3 m_Velocity;

	bool	 m_caughtStatus = false;
	bool	 m_messageDisplayed = false;
	bool	 m_closeToTarget = false;
	string	 m_bunnyText;
	Vector3  m_CirclePosition;
	
};