#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <stack>

using std::string;
using std::vector;
using std::stack;


void const printBitfield(char bitfield);
void const addToBitfield(char& bitfield, char bit);
void const DecimalToBinary(int decimal);
void const BinaryToDecimal(long long binary);
void const FlipBinary(int binary);

int const binToDec(const char* binaryString);
void const decToBin(char* binaryString, int len, int value);
void setBit(char& bitfield, char bit, bool flip);
bool checkBit(char& bitfield, char bit);

#define WEAPONS_COUNT 9;

vector<int> binaryArray;

const char CHAINSAW = 0x01;
const char PISTOL = 0x01 << 1;
const char SHOTGUN = 0x01 << 2;
const char SUPER_SHOTGUN = 0x01 << 3;
const char CHAINGUN = 0x01 << 4;
const char ROCKET_LAUNCHER = 0x01 << 5; 
const char PLASMA_GUN = 0x01 << 6;
const char BFG9000 = 0x01 << 7;

const char* weapons[] = {
		"Fists", "Chainsaw", "Pistol", "Shotgun", "Super Shotgun", "Chaingun",
"Rocket Launcher", "Plasma Gun", "BFG 9000"
};
