#include "pch.h"
#include "Main.h"
#include <iostream>

using std::cout;
using std::endl;
using std::cin;


int main()
{

	char bitfield = 0;

	binaryArray.reserve(10);

	addToBitfield(bitfield, CHAINSAW);
	addToBitfield(bitfield, SUPER_SHOTGUN);
	printBitfield(bitfield);

	cout << endl;
	FlipBinary(346);
	DecimalToBinary(20);

	BinaryToDecimal(11010);

	setBit(bitfield, SUPER_SHOTGUN, true);
	cout << endl;
	cout << "Is the bit set? " << checkBit(bitfield, SUPER_SHOTGUN) << endl;
	printBitfield(bitfield);

	const char* binaryString = "00001010"; // '0', '1', '0' === 48, 49, 48
	//char binaryString[9] = "01001010";

	cout << binToDec(binaryString) << endl;

	char* s = new char();
	decToBin(s, sizeof(s), 20);
	

	cin.get();

}

void const printBitfield(char bitfield) {

	cout << "Weapons set in bitfield: " << weapons[0] << " | ";
	for (unsigned int i = 1; i < 9; i++) {
		
		char mask = 0x01 << (i - 1);
		if ((bitfield & mask) == mask)
		{
			cout << weapons[i] << " | ";
		}
	}
	std::cout << std::endl;
}


void const addToBitfield(char& bitfield, char bit) {
	bitfield |= bit;
}


void const DecimalToBinary(int decimal)
{
	int temp = decimal;
	do {		
		binaryArray.push_back(decimal % 2);
		decimal /= 2;

	} while (decimal != 0);

	cout << "The binary of " << temp << " is ";
	for (int i = binaryArray.size() - 1; i >= 0; i--) {
		cout << binaryArray[i];
	}
	cout << endl;
}


void const BinaryToDecimal(long long binary)
{
	long long temp, bit, powerOf = 1, decimal = 0;
	
	temp = binary;
	while (temp > 0) {

		bit = temp % 10; // grab the bit of our current number
		temp /= 10; // move onto next bit
		decimal += (bit * powerOf);
		powerOf *= 2;	
	}

	cout << "The decimal of " << binary << " is " << decimal << endl;
}


void const FlipBinary(int decimal)
{
	int result = (~decimal + 1);
	cout << "Flipping " << decimal << " resulting in " << result << endl;
}






void setBit(char & bitfield, char bit, bool flip = false)
{
	if (!flip)
		bitfield |= bit;
	else
		bitfield &= ~bit;
}


bool checkBit(char & bitfield, char bit)
{
	unsigned char temp = bitfield & bit;
	return (temp != 0);
}


int const binToDec(const char * binaryString)
{
	if (binaryString == nullptr)
		throw "Invalid Input";

	stack<char> chars;
	while (*binaryString != '\0') 
	{
		chars.push(*binaryString - '0');
		binaryString++; // Increments the pointer
	}

	int value = 0;
	int powerof = 1;

	// Calculate value of decimal
	while (!chars.empty()) {
		value += (chars.top() * powerof);
		chars.pop();
		powerof *= 2;
	}

	return value;

}

void const decToBin(char * binaryString, int len, int value)
{
	if (binaryString == nullptr || len < 1)
		throw ("Invalid Input", " String Length: ", len);

	memset(binaryString, '0', len);

	stack<char> chars;

	// Take value and convert to decimal numbers, then insert into characters stack
	while (value != 0) {
		char bit = value % 2;
		value /= 2;
		chars.push('0' + bit); 
	}

	size_t index = 0;
	size_t charSize = chars.size();

	// Insert binary values into binaryString
	for (index; index < charSize; ++index) {
		binaryString[index] = chars.top();
		chars.pop();
	}

	// Insert null terminator at end
	binaryString[index] = '\0';

	for (int i = 0; i < sizeof(binaryString); ++i)
	{
		cout << binaryString[i];

		if (binaryString[i] == '\0')
			break;
	}
}