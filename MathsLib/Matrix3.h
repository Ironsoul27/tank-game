#pragma once
#include "Vector3.h"
#include <math.h>

class Matrix3
{
public:
	Matrix3();

	Matrix3(float x_x, float x_y, float x_z,
		float y_x, float y_y, float y_z,
		float z_x, float z_y, float z_z);


	~Matrix3();

	Vector3& operator[] (int index);
	const Vector3& operator[] (int index) const;

	Matrix3& operator= (const Matrix3& other);
	Matrix3 operator* (const Matrix3& other) const;
	Vector3 operator* (const Vector3& v) const;
	Matrix3 transposed() const;
	
	void setScaled(float x, float y, float z);
	void setScaled(const Vector3& v);
	void scale(float x, float y, float z);
	void scale(const Vector3& v);
	void setRotateX(float radians);
	void setRotateY(float radians);
	void setRotateZ(float radians);
	void rotateX(float radians);
	void rotateY(float radians);
	void rotateZ(float radians);
	void setEuler(float pitch, float yaw, float roll);
	void translate(float x, float y);

	operator float* ();
	operator const float* () const;

public:
	union {
		struct {
			Vector3 xAxis;
			Vector3 yAxis;
			union {
				Vector3 zAxis;
				Vector3 translation;
			};
		};
		Vector3 axis[3];
		float data[3][3];
		float m[9];
	};

	static Matrix3 identity;
};

//const Matrix3 Matrix3::identity = Matrix3(1, 0, 0, 0, 1, 0, 0, 0, 1);
