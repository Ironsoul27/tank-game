#pragma once
#include <cmath>
class Vector2
{
public:
	Vector2();
	Vector2(float x, float y);
	~Vector2();

public:
	float operator[] (int index) const;
	float& operator[] (int index);

	Vector2 operator+ (const Vector2& other) const;
	Vector2 operator- (float scalar) const;
	//float operator- (const Vector2& other) const;
	Vector2& operator- (const Vector2& other);
	Vector2 operator* (float scalar) const;
	Vector2 operator/ (float scalar) const;

	Vector2& operator= (const Vector2& other);
	Vector2& operator+= (const Vector2& other);
	Vector2& operator-= (const Vector2& other);
	Vector2& operator*= (const Vector2& other);
	Vector2& operator/= (const Vector2& other);

	float magnitudeSqr() const;
	float distance(const Vector2& other) const;
	void normalise();
	Vector2 normalised() const;
	float magnitude() const;
	float dot(const Vector2& other) const;
	Vector2 getPerpendicularRH() const;
	Vector2& clamp(const float clamp_value);

	operator float* ();
	operator const float* () const;

public:
	union
	{
		struct {
			float x, y;
		};

		float data[2];
	};	
};

Vector2 operator* (float scalar, Vector2& v);

