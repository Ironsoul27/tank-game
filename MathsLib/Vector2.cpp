#include "stdafx.h"
#include "Vector2.h"


Vector2::Vector2()
{
	 x = y = 0; 
}

Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
};


Vector2::~Vector2()
{
}

Vector2 operator* (float scalar, Vector2& v) {      
	return { v * scalar };
}

float Vector2::operator[] (int index) const { return data[index]; }
float& Vector2::operator[] (int index) { return data[index]; }

Vector2 Vector2::operator+ (const Vector2& other) const {
	return { x + other.x, y + other.y };
}

Vector2 Vector2::operator- (float scalar) const {
	return { x - scalar, y - scalar };
}

Vector2 Vector2::operator* (float scalar) const {
	return { x * scalar, y * scalar };
}

Vector2 Vector2::operator/ (float scalar) const {
	return { x / scalar, y / scalar };
}

Vector2& Vector2::operator= (const Vector2& other) {
	x = other.x; y = other.y;
	return *this;
}

Vector2& Vector2::operator+= (const Vector2& other) {
	x += other.x, y += other.y;
	return *this;
}

Vector2& Vector2::operator-= (const Vector2& other) {
	x -= other.x; y -= other.y;
	return *this;
}

Vector2& Vector2::operator*= (const Vector2& other) {
	x *= other.x; y *= other.y;
	return *this;
}

Vector2& Vector2::operator/= (const Vector2& other) {
	x /= other.x; y /= other.y;
	return *this;
}

//Vector2& Vector2::operator=(const Vector3& other)
//{
//	x = other.x, y = other.y;
//}

Vector2& Vector2::operator- (const Vector2& other) {
	x = x - other.x;
	y = y - other.y;
	return *this;
}

float Vector2::magnitudeSqr() const { return (x * x + y * y); }


float Vector2::distance(const Vector2& other) const {
	float diffx = x - other.x;
	float diffy = y - other.y;
	return sqrt(diffx * diffx + diffy * diffy);
}

void Vector2::normalise() {
	float mag = sqrt(x * x + y * y);
	x /= mag;
	y /= mag;
}

Vector2 Vector2::normalised() const {
	float mag = sqrt(x * x + y * y);
	return { x / mag, y / mag };
}

float Vector2::magnitude() const {
	return sqrt(x * x + y * y);
}

float Vector2::dot(const Vector2& other) const {
	return x * other.x + y * other.y;
}

Vector2 Vector2::getPerpendicularRH() const {
	return { -y, x };
}

Vector2& Vector2::clamp(const float clamp_value)
{
	x *= clamp_value, y *= clamp_value;
	return *this;
}

Vector2::operator float* () { return &x; }
Vector2::operator const float* () const { return &x; }