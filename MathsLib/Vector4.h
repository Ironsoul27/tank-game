#pragma once
#include <cmath>
#include "Vector3.h"
class Vector4
{
public:
	Vector4();
	Vector4(float x, float y, float z, float w);
	~Vector4();

public:
	float operator[] (int index) const;
	float& operator[] (int index);

	Vector4 operator+ (const Vector4& other) const;
	Vector4 operator- (float scalar) const;
	Vector4& operator- (const Vector4& other);
	Vector4 operator* (float scalar) const;
	Vector4 operator/ (float scalar);

	Vector4& operator= (const Vector4& other);

	Vector4& operator+= (const Vector4& other);
	Vector4& operator-= (const Vector4& other);
	Vector4& operator*= (const Vector4& other);
	Vector4& operator/= (const Vector4& other);
	Vector4& operator *=  (float scalar);


	float magnitude() const;
	void normalise();
	float dot(const Vector4& other) const;
	Vector4 cross(const Vector4& other) const;
	Vector4& operator= (const Vector3& other);

	operator float* ();
	operator const float* () const;

public:
	union
	{
		struct {
			float x, y, z, w;
		};

		float data[4];
	};
};

Vector4 operator* (float scalar, Vector4& v);
